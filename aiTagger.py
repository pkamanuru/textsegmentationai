import nltk, copy, random

#$$$ getting tagged as this
delimiter = "$$$"
delimiter_tag = "``"

#maintaining a list of all tags in training data
all_pos_tags = []
delimiting_pos_tags = []
max_sequence_length = 0

input_file = "100_samples"
input_data_filename = input_file + "ascii"


def delete_delimiter_from_words(words):
    while delimiter in words:
        index = words.index(delimiter)
        del words[index]
    return words

def convert_to_ascii(line):
    line = line.decode("utf-8")
    line = line.encode("ascii","ignore")
    return line

def tag_states_for_sequence_of_length(n):
    start = (n*(n+1) )/2
    tagged_sequence = []
    for i in range(start, start+n):
        tagged_sequence.append(i)

def tag_output_states_for_pos_tags(pos_tags):
    return [ all_pos_tags.index(tag[1]) for tag in pos_tags]
    
def add_tag_to_delimiting_pos_tags(tag):
    if tag[1] not in delimiting_pos_tags:
        delimiting_pos_tags.append(tag[1])
    
def add_tags_to_list ( list_of_tags ):
    for tag in list_of_tags:
        if tag[1] != delimiter_tag and tag[1] not in all_pos_tags:
            all_pos_tags.append(tag[1])

training_data =open( input_file, "r" )
##Generating Corresponding Tags
for line in training_data:
    line = line.rstrip()
    line = convert_to_ascii(line)
    words = line.split(' ')
    words.pop(0)
    
    indices = [i for i in range(len(words)) if words[i] == delimiter]
    words = delete_delimiter_from_words(words)
    tagged_words = nltk.pos_tag(words)
    add_tags_to_list( tagged_words )
    #find delimiter indices
    
    if len(indices) == 2:
        sequence_length = indices[1] - indices[0] - 1
        #Update Sequence Length
        if sequence_length > max_sequence_length:
            max_sequence_length = sequence_length
        #Next tag of second delimiter - $$$ which will important tag for the transition
        if len( tagged_words ) > indices[1]+1:
            add_tag_to_delimiting_pos_tags( tagged_words[sequence_length] )
    else:
        continue

training_data.close()


state_transition_sequences = []
output_state_sequences = []
#Removes $$$ from the list
def delete_delimiters_and_segment_text(words):
    indices = [i for i in range(len(words)) if words[i] == delimiter]
    if len(indices) < 2:
        print words
    segmented_text = copy.deepcopy( words[indices[0]+1 : indices[1]] )
    delimiter_text = words[indices[1]+1]
    remaining_text = words[indices[1]+2:]
    
    temp_words = copy.deepcopy(words)
    while delimiter in temp_words:
        index = temp_words.index(delimiter)
        del temp_words[index]
    return temp_words, segmented_text, delimiter_text, remaining_text

training_data =open( input_file, "r" )

excess_text_state = ( max_sequence_length*(max_sequence_length + 1) )/2 + 1
rejected_text_state = excess_text_state + len(delimiting_pos_tags) + 2

##Generating Corresponding Tags
for line in training_data:

    line = line.rstrip()
    line = convert_to_ascii(line)  
    words = line.split(' ')
    words.pop(0)

    if words[0] != delimiter:
        words.insert(0, delimiter)
    
    temp_words = copy.deepcopy(words)
    #clean_text - split into words in a list without $$$
    #segmented_text - first segment of text for each line embraced within $$$
    #delimiter_text - the word that seperates the segmented_text and remaining_text
    #remaining_text - reamining text that is consumed by the last state in the HMM
    
    clean_text, segmented_text, delimiter_text, remaining_text = delete_delimiters_and_segment_text(temp_words)
    pos_tag_clean_text = nltk.pos_tag(clean_text)

    #tagging the output states with ids which has a one to one mapping with pos_tags
    output_states = tag_output_states_for_pos_tags( nltk.pos_tag(clean_text) )
    
    #now we have to generate multiple state transition sequences
    length_of_seq = len(segmented_text)
    
    #Get pos tag of corresponding delimiter
    delimiter_pos_tag = pos_tag_clean_text[ clean_text.index(delimiter_text) ]
    if delimiter_pos_tag[1] not in delimiting_pos_tags:
        #Code will never come here
        indices = [i for i in range(len(words)) if temp_words[i] == delimiter]
        pp = pos_tag_clean_text[ clean_text.index(delimiter_text) ][1]
        break
    delimiter_text_state = excess_text_state + 1 + delimiting_pos_tags.index(delimiter_pos_tag[1])
    
    state_sequences = []
    
    for length in range(1, length_of_seq+1):
        #state seq for different lengths
        starting_state = ( (length-1)*length )/2
        length_state_seq = [starting_state + index for index in range(length) ]
        
        #excess text
        excess_text_left = length_of_seq - length
        excess_state_seq = [excess_text_state for index in range(excess_text_left) ]
        
        #delimiter
        delimiter_state_seq = [delimiter_text_state]
        
        #rejected state sequence
        rejected_state_seq = [ rejected_text_state for index in range(len(remaining_text)) ]
            
        state_transition_sequences.append(length_state_seq + excess_state_seq + delimiter_state_seq + rejected_state_seq)
        output_state_sequences.append(output_states)
        #state_sequences.append( length_state_seq + excess_state_seq + delimiter_state_seq + rejected_state_seq )
    
#     print len(state_sequences)
#     for seq in state_sequences:
#          print seq
#     print "###############"
#     print output_states
    #Just doing it for one example
#     break
training_data.close()
print len(state_sequences)

def write_list_to_file(list_of_seqs, file_name):
    f = open( file_name, "w" )
    for seq in list_of_seqs:
        f.write(str(1) + " ")
        for state in seq:
            f.write(str(state+2) + " ")
        f.write("\n")
    f.close()

write_list_to_file(output_state_sequences, "output_state_matlab")
write_list_to_file(state_transition_sequences, "state_transition_matlab")

no_of_states = rejected_text_state + 1
no_of_output_symbols = len(all_pos_tags)
prob = 1/float(no_of_output_symbols)
f = open("emission_probabilities", 'w')

for row in range(no_of_states+1):
    for col in range(no_of_output_symbols+1):
        f.write(str(prob)+" ")
    f.write("\n")

f.close()

prob = 1/float(no_of_states)
f = open("state_transitions", 'w')
for row in range(no_of_states+1):
    for col in range(no_of_states+1):
        f.write(str(prob)+" ")
    f.write("\n")
f.close()

max_state_len = 0
max_symb_len = 0
for seq in state_transition_sequences:
    max_state_len = max ( len(seq), max_state_len)

for seq in output_state_sequences:
    max_symb_len = max( len(seq), max_symb_len)

def prepare_training_data(state_transition_sequences, output_state_sequences):
    labelled_seq = []
    for i in range(len(state_transition_sequences)):
    #labelled_seq.append( (state_transition_sequences[i], output_state_sequences[i]) )
        cur_labelled_seq = []
        for j in range(len(state_transition_sequences[i])):
            cur_labelled_seq.append( (output_state_sequences[i][j], state_transition_sequences[i][j]) )
        labelled_seq.append(cur_labelled_seq)
    return labelled_seq

import nltk.tag.hmm as hmm
all_output = [i for i in range(len(all_pos_tags))]
all_states = [i for i in range(rejected_text_state)]
hmm_trainer = hmm.HiddenMarkovModelTrainer(all_states, all_output)
shuffle_list = [i for i in range(len(state_transition_sequences))]
random.shuffle(shuffle_list)

state_transition_sequences_mod = [ state_transition_sequences[i] for i in shuffle_list ]
output_state_sequences_mod = [ output_state_sequences[i] for i in shuffle_list ]

split_point = 100
labelled_seq = prepare_training_data(state_transition_sequences_mod[0:split_point], output_state_sequences_mod[0:split_point])
hmm_tagger = hmm_trainer.train_supervised(labelled_seq)

def no_of_rejected_states(states):
    ans = 0
    for ind in reversed(range(len(states))):
        if states[ind] == rejected_text_state:
            ans += 1
        else:
            break
    return ans
            
total_present = 0
no_of_matches = 0

for i in range(split_point, len(output_state_sequences)):
    label_pred = hmm_tagger.tag(output_state_sequences_mod[i])
    pred_state = [ pred[1] for pred in label_pred ]
    org_state = state_transition_sequences_mod[i]

    no_of_rejects_pred = no_of_rejected_states(pred_state)
    no_of_rejects_org = no_of_rejected_states(org_state)
    
    total_present += max(no_of_rejects_pred, no_of_rejects_org)
    no_of_matches += min(no_of_rejects_pred, no_of_rejects_org)
         
#Printing Accuracy of the predicted states 
print (no_of_matches/float(total_present))*100
