import random, nltk, copy
import nltk.tag.hmm as hmm

#Helper Functions to Create Training Data
def convert_to_ascii(line):
    line = line.decode("utf-8")
    line = line.encode("ascii","ignore")
    return line

#Note that we have a space as well
delimiter = ' $$$'

#Creates Cleaning(removing delimter) Trainingdata and indices to operate on
def generateCleanData(fileName, noOfTrainingDataPoints):
        filePtr = open(fileName, 'r')
        fileContents = filePtr.read()
        filePtr.close()
        
        data = []
        dataIndices = []
        for line in fileContents.split('\n'):
            line = line.rstrip()
            line = convert_to_ascii(line)
            lineIndex = line.find(delimiter)
            dataIndices.append(lineIndex)
            line = line[0:lineIndex]  + line[lineIndex+len(delimiter):]
            data.append(line)
        
        randomIndices = [i for i in range(len(data))]
        random.shuffle(randomIndices)
        
        data = [data[i] for i in randomIndices]
        dataIndices = [dataIndices[i] for i in randomIndices]
        
        return data[0:noOfTrainingDataPoints], dataIndices[0:noOfTrainingDataPoints], data[noOfTrainingDataPoints:], dataIndices[noOfTrainingDataPoints]        

trainingDataFile = '100_samples_refactored'

#Meta Data Generating Function
def generateMetaData(trainingData, trainingDataIndices):
        #Important Meta Data for Generating the following-
        #Output Sequences
        #State Sequences
        all_pos_tags = []
        delimiting_pos_tags = []
        max_sequence_length = 0
        
        def add_tags_to_list ( list_of_tags ):
                for tag in list_of_tags:
                        if tag[1] not in all_pos_tags:
                                all_pos_tags.append(tag[1])
        
        def add_tag_to_delimiting_pos_tags(tag):
                if tag[1] not in delimiting_pos_tags:
                        delimiting_pos_tags.append(tag[1])
        
        curIndex = 0
        for line in trainingData:
                
                splitPoint = 0
                if len(trainingDataIndices) > 0:
                        splitPoint = trainingDataIndices[curIndex]
                
                wordsFirstPart = line[0:splitPoint].split(' ')
                wordsSecondPart = line[splitPoint:].split(' ')
                words = wordsFirstPart + wordsSecondPart
                
                sequenceLength = len(wordsFirstPart)
                if sequenceLength > max_sequence_length:
                        max_sequence_length = sequenceLength
                
                #POS TAG per word ( Used by HMM to learn Patterns )
                tagged_words = nltk.pos_tag(words)
                
                #POS Tag of the first word in secondPart
                #Test data is also passed to add to all_pos_tags
                #Distinguishing Test data without indices
                if len(wordsSecondPart) > 0 and len(trainingDataIndices) > 0:
                        add_tag_to_delimiting_pos_tags(tagged_words[sequenceLength])
                        
                #Maintains the list of unique POS Tags found so far
                add_tags_to_list( tagged_words )
                
                curIndex += 1
        return all_pos_tags, delimiting_pos_tags, max_sequence_length

state_transition_sequences = []
output_state_sequences = []

def tag_output_states_for_pos_tags(pos_tags, all_pos_tags):
    return [ all_pos_tags.index(tag[1]) for tag in pos_tags]

def segmentText(line, words, split_index):
    segmented_text = copy.deepcopy( words[0 : split_index] )
    if len(words) <= split_index:
        print(words)
        print(split_index)
    delimiter_text = copy.deepcopy(words[split_index])
    remaining_text = copy.deepcopy(words[split_index+1:])

    return segmented_text, delimiter_text, remaining_text


def generateTrainingDataSequences(trainingData, trainingDataIndices, max_sequence_length, delimiting_pos_tags, all_pos_tags):
        excess_text_state = ( max_sequence_length*(max_sequence_length + 1) )/2 + 1
        rejected_text_state = excess_text_state + len(delimiting_pos_tags) + 2

        ##Generating Corresponding Tags for each line of text in training data
        curIndex = 0
        for line in trainingData:
            words = line.split(' ')
            #temp_words = copy.deepcopy(words)
            #clean_text - split into words in a list without $$$
            #segmented_text - first segment of text for each line embraced within $$$
            #delimiter_text - the word that seperates the segmented_text and remaining_text
            #remaining_text - reamining text that is consumed by the last state in the HMM

            segmented_text, delimiter_text, remaining_text = segmentText(line, words, trainingDataIndices[curIndex])
            pos_tag_clean_text = nltk.pos_tag(words)

            #tagging the output states with ids which has a one to one mapping with pos_tags
            output_states = tag_output_states_for_pos_tags( nltk.pos_tag(words), all_pos_tags )

            #now we have to generate multiple state transition sequences
            length_of_seq = len(segmented_text)

            #Get pos tag of corresponding delimiter
            delimiter_pos_tag = pos_tag_clean_text[ words.index(delimiter_text) ]
            if delimiter_pos_tag[1] not in delimiting_pos_tags:
                #Code will never come here
                #indices = [i for i in range(len(words)) if temp_words[i] == delimiter]
                pp = pos_tag_clean_text[ words.index(delimiter_text) ][1]
                break
            delimiter_text_state = excess_text_state + 1 + delimiting_pos_tags.index(delimiter_pos_tag[1])

            state_sequences = []

            #Generating state transition sequences and output sequences of all lengths for each data point
            for length in range(1, length_of_seq+1):
                #state seq for different lengths
                starting_state = ( (length-1)*length )/2
                length_state_seq = [starting_state + index for index in range(length) ]

                #excess text
                excess_text_left = length_of_seq - length
                excess_state_seq = [excess_text_state for index in range(excess_text_left) ]

                #delimiter
                delimiter_state_seq = [delimiter_text_state]

                #rejected state sequence
                rejected_state_seq = [ rejected_text_state for index in range(len(remaining_text)) ]

                state_transition_sequences.append(length_state_seq + excess_state_seq + delimiter_state_seq + rejected_state_seq)
                output_state_sequences.append(output_states)
                
                
            curIndex += 1
        return state_transition_sequences, output_state_sequences, rejected_text_state
    
def prepare_training_data(state_transition_sequences, output_state_sequences):
    labelled_seq = []
    for i in range(len(state_transition_sequences)):
    #labelled_seq.append( (state_transition_sequences[i], output_state_sequences[i]) )
        cur_labelled_seq = []
        for j in range(len(state_transition_sequences[i])):
            cur_labelled_seq.append( (output_state_sequences[i][j], state_transition_sequences[i][j]) )
        labelled_seq.append(cur_labelled_seq)
    return labelled_seq

def trainHMM(all_pos_tags, rejected_text_state, state_transition_sequences, output_state_sequences):
        all_output = [i for i in range(len(all_pos_tags))]
        all_states = [i for i in range(rejected_text_state)]
        hmm_trainer = hmm.HiddenMarkovModelTrainer(all_states, all_output)
        shuffle_list = [i for i in range(len(state_transition_sequences))]
        random.shuffle(shuffle_list)

        state_transition_sequences_mod = [ state_transition_sequences[i] for i in shuffle_list ]
        output_state_sequences_mod = [ output_state_sequences[i] for i in shuffle_list ]

        split_point = 100
        labelled_seq = prepare_training_data(state_transition_sequences_mod[0:split_point], output_state_sequences_mod[0:split_point])
        hmm_tagger = hmm_trainer.train_supervised(labelled_seq)
        return hmm_tagger

def calculateIndex(hmm_tagger, testDataLine, rejected_text_state, all_pos_tags):
        def no_of_rejected_states(states):
            ans = 0
            for ind in reversed(range(len(states))):
                if states[ind] == rejected_text_state:
                    ans += 1
                else:
                    break
            return ans
        
        words = testDataLine.split(' ')
        output_states = tag_output_states_for_pos_tags( nltk.pos_tag(words), all_pos_tags )
        labelled_states = hmm_tagger.tag(output_states)
        predicted_states = [ state[1] for state in labelled_states]
        no_of_rejected_states = no_of_rejected_states(predicted_states)
        
        noOfSegmentedWords = len(words) - no_of_rejected_states - 1#The delimiting pos tag state
        segmentedIndex = 0
        for i in range(0, noOfSegmentedWords):
            segmentedIndex += len(words[i]) + 1
        segmentedIndex -= 1
        
        return segmentedIndex
        
        
        
def calculateTestIndices(trainingData, trainingDataIndices, testData):
        all_pos_tags, delimiting_pos_tags, max_sequence_length = generateMetaData(trainingData, trainingDataIndices)
        test_data_pos_tags, test_delimiting_pos_tags, test_max_sequence_length = generateMetaData(testData, [])
        
        #Appending test Pos Tags to all Pos Tags so that we  handle every pos tag
        for pos_tag in test_data_pos_tags:
                if pos_tag not in all_pos_tags:
                        all_pos_tags.append(pos_tag)
        print(len(all_pos_tags))
        print(all_pos_tags)
                        
        state_transition_sequences, output_state_sequences, rejected_text_state = generateTrainingDataSequences(trainingData, trainingDataIndices, max_sequence_length, delimiting_pos_tags, all_pos_tags)
        hmm_tagger = trainHMM(all_pos_tags, rejected_text_state, state_transition_sequences, output_state_sequences)
        
        testIndices = []
        for testDataLine in testData:
                testIndices.append(calculateIndex(hmm_tagger, testDataLine, rejected_text_state, all_pos_tags))
        return testIndices

trainingData, trainingDataIndices, testData, testDataIndices = generateCleanData(trainingDataFile, 10)
predictedTestDataIndices = calculateTestIndices(trainingData, trainingDataIndices, testData)
